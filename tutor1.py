""" Raspberry Pi / Grove Pi Tutorial 1 - Led/Sound/Touch """
import smbus
import time
import grovepi

# Constants, because abstraction is delicious

HIGH = 1 # On signal
LOW = 0  # Off Signal
BUS = smbus.SMBus(0)  # for RPI version 1, use "bus = smbus.SMBus(0)"
ADDRESS = 0x04  # This is the address we setup in the Arduino Program

#pin setup
A0 = 0
A1 = 1
A2 = 2

D2 = 2
D3 = 3
D4 = 4
D5 = 5
D6 = 6
D7 = 7
D8 = 8


# Component Connections

button_input = A0
touch_input = A1

buzzer_output = D3
led_output = D4


grovepi.pinMode(led_output,"OUTPUT") 
grovepi.pinMode(buzzer_output,"OUTPUT") 


keep_running = True  # variable to control ending of the loop
LED_status = LOW  # initialize the LED to be off

while keep_running:
	print ("Yup Still Running")
	button_press = grovepi.analogRead(button_input) or 0
	print ("Button press value:"+str(button_press))
	touch_press = grovepi.analogRead(touch_input) or 0
	print ("Touch press value:"+str(touch_press))
	
	if button_press > 0 and touch_press > 0:
		keep_running = False
	elif button_press > 0:  # toggle light
		if LED_status == LOW:
			LED_status = HIGH
		else:
			LED_status = LOW
		grovepi.digitalWrite(led_output,LED_status)
	elif touch_press > 0:
		grovepi.digitalWrite(buzzer_output,HIGH)
		time.sleep(.1)
		grovepi.digitalWrite(buzzer_output,LOW)

grovepi.digitalWrite(led_output,LOW)
grovepi.digitalWrite(buzzer_output,LOW)