#Grove pi test with Pot and LED
import smbus
import time
import grovepi
# for RPI version 1, use "bus = smbus.SMBus(0)"
bus = smbus.SMBus(0)
 
# This is the address we setup in the Arduino Program
address = 0x04
 
#Set pin 5 as OUTPUT on the GrovePi
grovepi.pinMode(5,"OUTPUT")
time.sleep(1)
i=0
while True:
	try:
		#Read value of the rotary encoder on Analog Port2
		i=grovepi.analogRead(2)
		print i
		#Send the PWM signal on Digital port 5 for LED fade
		grovepi.analogWrite(5,i/4)
	except IOError:
		print "Error"